from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import ListForm, ItemForm


# this shows all the todo lists
def todo_list(request):
    list = TodoList.objects.all()
    context = {
        "todo_list_list": list,
    }
    return render(request, "todos/list.html", context)


# this shows the name of a specific todo list
def todo_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detail": detail,
    }
    return render(request, "todos/detail.html", context)


# this shows a create view
def create_list(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = ListForm()
    context = {
        "create_form": form,
    }
    return render(request, "todos/create.html", context)


def edit_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=list)
        if form.is_valid():
            listid = form.save()
            return redirect("todo_list_detail", id=listid.id)
    else:
        form = ListForm(instance=list)

    context = {
        "list_object": list,
        "list_form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_list(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")
    else:
        pass

    return render(request, "todos/delete.html")


def item_create(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/createitem.html", context)


def edit_item(request, id):
    list_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=list_item)
        if form.is_valid():
            itemid = form.save()
            return redirect("todo_list_detail", id=itemid.list.id)
    else:
        form = ItemForm(instance=list_item)

    context = {
        "list_item": list_item,
        "item_form": form,
    }
    return render(request, "todos/itemedit.html", context)
