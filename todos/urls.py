from django.urls import path
from .views import (
    todo_list,
    todo_detail,
    create_list,
    edit_list,
    delete_list,
    item_create,
    edit_item,
)


urlpatterns = [
    path("todos/", todo_list, name="todo_list_list"),
    path("todos/<int:id>/", todo_detail, name="todo_list_detail"),
    path("todos/create/", create_list, name="todo_list_create"),
    path("todos/<int:id>/edit/", edit_list, name="todo_list_update"),
    path("todos/<int:id>/delete/", delete_list, name="todo_list_delete"),
    path("todos/items/create/", item_create, name="todo_item_create"),
    path("todos/items/<int:id>/edit/", edit_item, name="todo_item_update"),
]
